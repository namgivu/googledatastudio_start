import os

from flask import Flask, render_template, redirect, url_for


app = Flask(__name__)


def load_ggds_chart_embedurl():
    """
    ref. demo ggds chart w/ ggsheet
    ggds    https://datastudio.google.com/s/jZinG9mSIvY
    ggsheet https://docs.google.com/spreadsheets/d/1ZEapszcFGqfBkwRVZ1SXumWfSsOITpVDTL4419NlRes/edit?usp=sharing
    """
    ggds_chart_embedurl = 'https://datastudio.google.com/embed/reporting/9202dad2-a431-4c1f-b121-5a6080a955ea/page/maBrC'
    return ggds_chart_embedurl


@app.route('/')
def index():
    return redirect(url_for('t02_ggds_iframe'), **locals())


@app.route('/t00_iframe_bw_ggds_vs_ours')
def t00_iframe_bw_ggds_vs_ours():
    ggds_chart_embedurl = load_ggds_chart_embedurl()
    return render_template('t00_iframe_bw_ggds_vs_ours.html', **locals())


@app.route('/t01_ours_iframe')
def t01_ours_iframe():
    return render_template('t01_ours_iframe.html', **locals())


@app.route('/t02_ggds_iframe')
def t02_ggds_iframe():
    ggds_chart_embedurl = load_ggds_chart_embedurl()
    return render_template('t02_ggds_iframe.html', **locals())


if __name__ == '__main__':
    APP_PORT = os.environ.get('APP_PORT', 5000)  # 5000 is flask default runport

    app.run(port=APP_PORT, host='0.0.0.0', debug=True)
    #       p              h               d=debug mode so that can update html template at runtime
